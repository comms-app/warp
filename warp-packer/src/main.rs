#[macro_use]
extern crate lazy_static;

use clap::{command, Arg};
use flate2::write::GzEncoder;
use flate2::Compression;
use hex_literal::hex;
use sha2::{Digest, Sha256};
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::fs::File;
use std::io;
use std::io::Write;
use std::io::{copy, Read};
use std::path::{Path, PathBuf};
use std::process;
use tempdir::TempDir;

const APP_NAME: &str = env!("CARGO_PKG_NAME");

const FILENAME_MAGIC: &[u8] = b"tVQhhsFFlGGD3oWV4lEPST8I8FEPP54IM0q7daes4E1y3p2U2wlJRYmWmjPYfkhZ0PlT14Ls0j8fdDkoj33f2BlRJavLj3mWGibJsGt5uLAtrCDtvxikZ8UX2mQDCrgE\0";
const APP_ID_MAGIC: &[u8] = b"83h5sxql9z6tj43vfbqobw6y92y5fzi4md7l7vfedgjbgtk2eq9aa5692y47ol0a8efsttlb9gstwmx56sofc05lr7l2w5dw30n49oaypbfrnfy2v1eb693tz9o4af25\0";
const HASH_MAGIC: [u8; 32] =
    hex!("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");

const RUNNER_WINDOWS_X64: &[u8] =
    include_bytes!("../../target/x86_64-pc-windows-msvc/release/warp-runner.exe");

lazy_static! {
    static ref RUNNER_BY_ARCH: HashMap<&'static str, &'static [u8]> = {
        let mut m = HashMap::new();
        m.insert("windows-x64", RUNNER_WINDOWS_X64);
        m
    };
}

/// Print a message to stderr and exit with error code 1
macro_rules! bail {
    () => (process::exit(1));
    ($($arg:tt)*) => ({
        eprint!("{}\n", format_args!($($arg)*));
        process::exit(1);
    })
}

fn hash_file(exec_name: &PathBuf) -> io::Result<Vec<u8>> {
    let mut bytes = Vec::new();
    File::open(exec_name)?.read_to_end(&mut bytes)?;

    let mut hasher = Sha256::new();
    hasher.update(bytes);
    Ok(hasher.finalize().to_vec())
}

fn patch_runner_single(exec: &mut [u8], original_magic: &[u8], new_magic: &[u8]) -> io::Result<()> {
    let magic_len = original_magic.len();

    // Find the magic buffer offset inside the runner executable
    let mut offs_opt = None;
    for (i, chunk) in exec.windows(magic_len).enumerate() {
        if chunk == original_magic {
            offs_opt = Some(i);
            break;
        }
    }

    if offs_opt.is_none() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("could not find magic '{original_magic:X?}' found inside runner"),
        ));
    }

    // Replace the magic with the new one that points to the target executable
    let offs = offs_opt.unwrap();
    exec[offs..offs + magic_len].clone_from_slice(new_magic);

    Ok(())
}

fn patch_runner(
    arch: &str,
    exec_name: &str,
    exec_hash: &[u8],
    app_id: Option<&String>,
) -> io::Result<Vec<u8>> {
    // Read runner executable in memory
    let runner_contents = RUNNER_BY_ARCH.get(arch).unwrap();
    let mut buf = runner_contents.to_vec();

    // Set the correct target executable name and hash into the local magic buffers
    let mut filename_magic = vec![0; FILENAME_MAGIC.len()];
    filename_magic[..exec_name.len()].clone_from_slice(exec_name.as_bytes());
    patch_runner_single(&mut buf, FILENAME_MAGIC, &filename_magic)?;
    patch_runner_single(&mut buf, &HASH_MAGIC, exec_hash)?;

    let mut app_id_magic = vec![0; APP_ID_MAGIC.len()];
    if let Some(app_id) = app_id {
        app_id_magic[..app_id.len()].clone_from_slice(app_id.as_bytes());
    }
    // Still patch even if no app_id so that it's overwritten with zeroes
    patch_runner_single(&mut buf, APP_ID_MAGIC, &app_id_magic)?;

    Ok(buf)
}

fn create_tgz(dir: &Path, out: &Path) -> io::Result<()> {
    let f = fs::File::create(out)?;
    let gz = GzEncoder::new(f, Compression::best());
    let mut tar = tar::Builder::new(gz);
    tar.follow_symlinks(false);
    tar.append_dir_all(".", dir)?;
    Ok(())
}

#[cfg(target_family = "unix")]
fn create_app_file(out: &Path) -> io::Result<File> {
    use std::os::unix::fs::OpenOptionsExt;

    fs::OpenOptions::new()
        .create(true)
        .write(true)
        .mode(0o755)
        .open(out)
}

#[cfg(target_family = "windows")]
fn create_app_file(out: &Path) -> io::Result<File> {
    fs::OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .open(out)
}

fn create_app(runner_buf: &[u8], tgz_path: &Path, out: &Path) -> io::Result<()> {
    let mut outf = create_app_file(out)?;
    let mut tgzf = fs::File::open(tgz_path)?;
    outf.write_all(runner_buf)?;
    copy(&mut tgzf, &mut outf)?;
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = command!()
        .arg(
            Arg::new("arch")
                .short('a')
                .long("arch")
                .value_name("arch")
                .help(format!(
                    "Sets the architecture. Supported: {:?}",
                    RUNNER_BY_ARCH.keys()
                ))
                .display_order(1)
                .required(true),
        )
        .arg(
            Arg::new("input_dir")
                .short('i')
                .long("input_dir")
                .value_name("input_dir")
                .help("Sets the input directory containing the application and dependencies")
                .display_order(2)
                .required(true),
        )
        .arg(
            Arg::new("exec")
                .short('e')
                .long("exec")
                .value_name("exec")
                .help("Sets the application executable file name")
                .display_order(3)
                .required(true),
        )
        .arg(
            Arg::new("output")
                .short('o')
                .long("output")
                .value_name("output")
                .help("Sets the resulting self-contained application file name")
                .display_order(4)
                .required(true),
        )
        .arg(
            Arg::new("app_id")
                .short('d')
                .long("coalesce_window_with_app_id")
                .value_name("app_id")
                .help(
                    "To make Taskbar pinning refer to the host Warp binary instead of the \
                embedded one, Warp will start with a barebones minimized window, and pass the \
                embedded app the handle via \"WARP_WINDOW_HANDLE\" so you can WM_COMMAND to close \
                it when your window is shown. Sets \"Application User Model ID\" as provided, your \
                embedded app should do the same.",
                )
                .display_order(5)
                .required(false),
        )
        .get_matches();

    let arch = args.get_one::<String>("arch").unwrap().as_str();
    if !RUNNER_BY_ARCH.contains_key(arch) {
        bail!(
            "Unknown architecture specified: {}, supported: {:?}",
            arch,
            RUNNER_BY_ARCH.keys()
        );
    }

    let input_dir = Path::new(args.get_one::<String>("input_dir").unwrap());
    if fs::metadata(input_dir).is_err() {
        bail!("Cannot access specified input directory {:?}", input_dir);
    }

    let exec_name = args.get_one::<String>("exec").unwrap();
    if exec_name.len() >= FILENAME_MAGIC.len() {
        bail!("Executable name is too long, please consider using a shorter name");
    }

    let app_id = args.get_one::<String>("app_id");
    if let Some(app_id) = app_id {
        if app_id.len() >= APP_ID_MAGIC.len() {
            bail!("App ID is too long, please consider using a shorter ID");
        }
    }

    let exec_path = Path::new(input_dir).join(exec_name);
    match fs::metadata(&exec_path) {
        Err(_) => {
            bail!("Cannot find file {:?}", exec_path);
        }
        Ok(metadata) => {
            if !metadata.is_file() {
                bail!("{:?} isn't a file", exec_path);
            }
        }
    }

    println!("Compressing input directory {input_dir:?}...");
    let tmp_dir = TempDir::new(APP_NAME)?;
    let tgz_path = tmp_dir.path().join("input.tgz");
    create_tgz(input_dir, &tgz_path)?;

    let hash = hash_file(&tgz_path)?;
    let runner_buf = patch_runner(arch, exec_name, &hash, app_id)?;

    let exec_name = Path::new(args.get_one::<String>("output").unwrap());
    println!("Creating self-contained application binary {exec_name:?}...");
    create_app(&runner_buf, &tgz_path, exec_name)?;

    println!("All done");
    Ok(())
}
