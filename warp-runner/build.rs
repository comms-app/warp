fn main() -> std::io::Result<()> {
    #[cfg(all(windows, not(debug_assertions)))]
    {
        static_vcruntime::metabuild();
    }
    Ok(())
}
