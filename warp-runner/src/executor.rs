use std::env;
#[cfg(target_family = "unix")]
use std::fs;
#[cfg(target_family = "unix")]
use std::fs::Permissions;
use std::io;
#[cfg(target_family = "unix")]
use std::os::unix::fs::PermissionsExt;
use std::os::windows::io::AsRawHandle;
use std::path::Path;
use std::process::Command;
use std::process::Stdio;
use windows::Win32::Foundation;
use windows::Win32::Foundation::HWND;
use windows::Win32::UI::WindowsAndMessaging;
use windows::Win32::UI::WindowsAndMessaging::{DispatchMessageW, MSG};

pub fn execute(target: &Path, window: Option<HWND>) -> io::Result<i32> {
    trace!("target={:?}", target);

    let args: Vec<String> = env::args().skip(1).collect();
    trace!("args={:?}", args);

    do_execute(target, &args, window)
}

#[cfg(target_family = "unix")]
fn ensure_executable(target: &Path) {
    let perms = Permissions::from_mode(0o770);
    fs::set_permissions(target, perms).unwrap();
}

#[cfg(target_family = "unix")]
fn do_execute(target: &Path, args: &[String]) -> io::Result<i32> {
    ensure_executable(target);

    Ok(Command::new(target)
        .args(args)
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn()?
        .wait()?
        .code()
        .unwrap_or(1))
}

#[cfg(target_family = "windows")]
fn is_script(target: &Path) -> bool {
    const SCRIPT_EXTENSIONS: &[&str] = &["bat", "cmd"];
    SCRIPT_EXTENSIONS.contains(
        &target
            .extension()
            .unwrap_or_default()
            .to_string_lossy()
            .to_lowercase()
            .as_str(),
    )
}

#[cfg(target_family = "windows")]
fn do_execute(target: &Path, args: &[String], window: Option<HWND>) -> io::Result<i32> {
    let current_exe = env::current_exe()?;
    let configure_command = |command: &mut Command| {
        command
            .stdin(Stdio::inherit())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .env("WARP_WRAPPER_PATH", current_exe.as_path());
        if let Some(window) = window {
            let window = window.0.to_string();
            command.env("WARP_WINDOW_HANDLE", window);
        }
    };

    let mut child = if is_script(target) {
        let mut cmd_args = Vec::with_capacity(args.len() + 2);
        cmd_args.push("/c".to_string());
        cmd_args.push(target.to_string_lossy().to_string());
        cmd_args.extend_from_slice(args);

        let mut command = Command::new("cmd");
        configure_command(command.args(cmd_args));
        command.spawn()?
    } else {
        let mut command = Command::new(target);
        configure_command(command.args(args));
        command.spawn()?
    };

    if window.is_some() {
        // Wait for child to submit WM_CLOSE message.
        // The handler calls PostQuitMessage, which frees us from the GetMessageW loop.
        let handles = [Foundation::HANDLE(child.as_raw_handle() as isize)];

        loop {
            let return_value = unsafe {
                WindowsAndMessaging::MsgWaitForMultipleObjectsEx(
                    Some(&handles),
                    u32::MAX,
                    WindowsAndMessaging::QS_ALLINPUT,
                    WindowsAndMessaging::MWMO_NONE,
                )
            };

            match return_value {
                r if r == Foundation::WAIT_OBJECT_0 => {
                    return Ok(child.wait()?.code().unwrap_or(1))
                }
                r if r.0 == Foundation::WAIT_OBJECT_0.0 + handles.len() as u32 => {
                    let mut message = MSG::default();
                    if unsafe {
                        WindowsAndMessaging::PeekMessageW(
                            &mut message,
                            None,
                            0,
                            0,
                            WindowsAndMessaging::PM_REMOVE,
                        )
                    } == true
                    {
                        unsafe { DispatchMessageW(&message) };
                    }
                }
                r => {
                    panic!(
                        "MsgWaitForMultipleObjectsEx returned unexpected value \"{}\"",
                        r.0
                    );
                }
            }
        }
    }

    Ok(child.wait()?.code().unwrap_or(1))
}
