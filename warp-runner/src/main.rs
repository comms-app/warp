#[macro_use]
extern crate log;

use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::ffi::*;
use std::fs;
use std::fs::File;
use std::io;
use std::io::{ErrorKind, Write};
use std::mem::size_of;
use std::path::*;
use std::process;

use hex_literal::hex;
use log::Level;
use windows::core::w;
use windows::core::{HSTRING, PCWSTR};
use windows::Win32::Foundation::{HWND, LPARAM, LRESULT, MAX_PATH, WPARAM};
use windows::Win32::System::LibraryLoader::{GetModuleFileNameW, GetModuleHandleW};
use windows::Win32::UI::Shell::ExtractIconExW;
use windows::Win32::UI::Shell::SetCurrentProcessExplicitAppUserModelID;
use windows::Win32::UI::WindowsAndMessaging::{
    CreateWindowExW, DefWindowProcW, PostQuitMessage, RegisterClassExW, ShowWindow, CS_HREDRAW,
    CS_VREDRAW, SW_SHOWMINNOACTIVE, WM_DESTROY, WNDCLASSEXW, WS_MINIMIZE,
};

mod executor;
mod extractor;

static TARGET_FILE_NAME_BUF: &[u8] = b"tVQhhsFFlGGD3oWV4lEPST8I8FEPP54IM0q7daes4E1y3p2U2wlJRYmWmjPYfkhZ0PlT14Ls0j8fdDkoj33f2BlRJavLj3mWGibJsGt5uLAtrCDtvxikZ8UX2mQDCrgE\0";
static TARGET_APP_ID_BUF: &[u8] = b"83h5sxql9z6tj43vfbqobw6y92y5fzi4md7l7vfedgjbgtk2eq9aa5692y47ol0a8efsttlb9gstwmx56sofc05lr7l2w5dw30n49oaypbfrnfy2v1eb693tz9o4af25\0";
static TARGET_SHA256_HASH: [u8; 32] =
    hex!("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");

fn target_file_name() -> &'static str {
    let nul_pos = TARGET_FILE_NAME_BUF
        .iter()
        .position(|elem| *elem == b'\0')
        .expect("TARGET_FILE_NAME_BUF has no NUL terminator");

    let slice = &TARGET_FILE_NAME_BUF[..(nul_pos + 1)];
    CStr::from_bytes_with_nul(slice)
        .expect("Can't convert TARGET_FILE_NAME_BUF slice to CStr")
        .to_str()
        .expect("Can't convert TARGET_FILE_NAME_BUF CStr to str")
}

fn target_app_id() -> Option<&'static str> {
    let nul_pos = TARGET_APP_ID_BUF.iter().position(|elem| *elem == b'\0')?;
    if nul_pos == 0 {
        return None;
    }

    let slice = &TARGET_APP_ID_BUF[..(nul_pos + 1)];
    CStr::from_bytes_with_nul(slice).ok()?.to_str().ok()
}

fn scoped_data_dir() -> PathBuf {
    dirs::data_local_dir()
        .expect("No data local dir found")
        .join("warp")
}

fn with_state(
    data_dir: &Path,
    f: impl FnOnce(&mut HashMap<String, [u8; 32]>) -> io::Result<()>,
) -> Result<(), Box<dyn Error>> {
    let state_file_path = data_dir.join("state.bin");

    let mut state = match fs::read(&state_file_path) {
        Ok(bytes) => bincode::deserialize(&bytes)?,
        Err(e) if e.kind() == ErrorKind::NotFound => HashMap::new(),
        Err(e) => return Err(e.into()),
    };

    f(&mut state)?;
    let bytes = bincode::serialize(&state)?;
    let mut state_file = File::create(&state_file_path)?;
    state_file.write_all(&bytes)?;
    Ok(())
}

fn extract(exe_path: &Path, cache_path: &Path) -> io::Result<()> {
    fs::create_dir_all(cache_path)?;
    for entry_result in fs::read_dir(cache_path)? {
        let entry = entry_result?;
        match fs::remove_file(&entry.path()) {
            Ok(_) => {}
            Err(ref e) if e.kind() == ErrorKind::PermissionDenied => {
                // PermissionDenied may be because this file is actually a directory
                fs::remove_dir_all(&entry.path())?;
            }
            Err(e) => return Err(e),
        }
    }

    extractor::extract_to(exe_path, cache_path)?;
    Ok(())
}

extern "system" fn wndproc(window: HWND, message: u32, wparam: WPARAM, lparam: LPARAM) -> LRESULT {
    unsafe {
        match message {
            WM_DESTROY => {
                PostQuitMessage(0);
                LRESULT(0)
            }
            _ => DefWindowProcW(window, message, wparam, lparam),
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    if env::var("WARP_TRACE").is_ok() {
        simple_logger::init_with_level(Level::Trace)?;
    }

    let window = if let Some(app_id) = target_app_id() {
        trace!("app_id={:?}", app_id);
        let app_id_hstring: HSTRING = app_id.into();
        unsafe { SetCurrentProcessExplicitAppUserModelID(&app_id_hstring) }.unwrap();

        let instance = unsafe { GetModuleHandleW(PCWSTR::null()) }.unwrap();
        let window_class_name = w!("class");

        let mut window_class = WNDCLASSEXW {
            cbSize: size_of::<WNDCLASSEXW>() as u32,
            style: CS_HREDRAW | CS_VREDRAW,
            lpfnWndProc: Some(wndproc),
            hInstance: instance.into(),
            lpszClassName: window_class_name,
            ..Default::default()
        };

        let mut module_filename = [0u16; MAX_PATH as usize];
        unsafe { GetModuleFileNameW(None, &mut module_filename) };
        unsafe {
            ExtractIconExW(
                PCWSTR::from_raw(module_filename.as_ptr()),
                0,
                Some(&mut window_class.hIcon),
                Some(&mut window_class.hIconSm),
                1,
            )
        };

        let _atom = unsafe { RegisterClassExW(&window_class) };
        let window = unsafe {
            CreateWindowExW(
                Default::default(),
                window_class_name,
                w!("Warp"),
                WS_MINIMIZE,
                0,
                0,
                0,
                0,
                None,
                None,
                instance,
                None,
            )
        };
        let _ = unsafe { ShowWindow(window, SW_SHOWMINNOACTIVE) };
        Some(window)
    } else {
        None
    };

    let self_path = env::current_exe()?;
    let self_file_name = self_path.file_name().unwrap();
    let scoped_data_dir = scoped_data_dir();
    let cache_path = scoped_data_dir
        .join("packages")
        .join(&self_file_name.to_string_lossy() as &str);

    trace!("self_path={:?}", self_path);
    trace!("self_file_name={:?}", self_file_name);
    trace!("cache_path={:?}", cache_path);

    let target_file_name = target_file_name();
    let target_hash = &TARGET_SHA256_HASH;
    let target_path = cache_path.join(target_file_name);

    trace!("target_exec={:?}", target_file_name);
    trace!("target_hash={:X?}", target_hash);
    trace!("target_path={:?}", target_path);

    with_state(&scoped_data_dir, |state| {
        match state.get(target_file_name) {
            Some(hash) => {
                if hash == target_hash && target_path.exists() {
                    trace!("cache is up-to-date");
                } else {
                    trace!("cache is outdated");
                    extract(&self_path, &cache_path)?;
                }
            }
            None => {
                trace!("cache not found");
                extract(&self_path, &cache_path)?;
            }
        };
        state.insert(target_file_name.to_string(), *target_hash);
        Ok(())
    })?;

    let exit_code = executor::execute(&target_path, window)?;
    process::exit(exit_code);
}
